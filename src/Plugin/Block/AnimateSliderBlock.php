<?php

namespace Drupal\animate_slider_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;

/**
 * Provides a new animate slider block.
 *
 * @Block(
 *   id = "animate_slider",
 *   admin_label = @Translation("Block: animate slider")
 * )
 */
class AnimateSliderBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntityController.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'category' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $tids = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery()->condition('vid', 'slider')->execute();

    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($tids);
    $cat = [];
    foreach ($terms as $key => $term) {
      $cat[$key] = $term->toLink()->getText();
    }
    $form['category'] = [
      '#type' => 'select',
      '#title' => $this->t('Category'),
      '#options' => $cat,
      '#default_value' => $this->configuration['category'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['category'] = $form_state->getValue('category');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $nids = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', 'slider')
      ->condition('status', 1)
      ->condition('field_slider_type', $this->configuration['category'])
      ->execute();

    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);

    $slide_config = [];
    $data = [];
    foreach ($nodes as $node) {
      $slide_config[] = [
        '.animate-field-title' => [
          'show' => 'bounceIn',
          'hide' => 'zoomOutUpBig',
          'delayShow' => 'delay1s',
        ],
        '.animate-field-body' => [
          'show' => 'fadeInUpLeftLarge',
          'hide' => 'fadeOutDownBig',
          'delayShow' => 'delay1-5s',
        ],
        '.animate-field-link' => [
          'show' => 'zoomInUpRightLarge',
          'hide' => 'fadeOutRightBig',
          'delayShow' => 'delay1-5s',
        ],
        'img#slideshow-image' => [
          'show' => 'rollIn',
          'hide' => 'flipOutX',
          'delayShow' => 'delay2s',
        ],
      ];
      $title = $node->title->value;
      $summary = $node->body->value;
      $image_storage = $this->entityTypeManager->getStorage('image_style');
      $img = $image_storage->load('medium')->buildUrl($node->field_slide_image->entity->getFileUri());
      $node_link = Link::fromTextAndUrl($this->t('Read more'), $node->toUrl('canonical'));
      $data[] = [
        'title' => $title,
        'summary' => $summary,
        'nodelink' => $node_link,
        'img' => $img,
      ];
    }

    $slide_id = $this->configuration['category'];
    $js_config[$slide_id] = ['config' => $slide_config];
    return [
      '#title' => 'Animate slider',
      '#theme' => 'animate_slider_block',
      '#id' => $slide_id,
      '#rows' => $data,
      '#attached' => [
        'library' => [
          'animate_slider_block/jquery.animate.slider',
        ],
        'drupalSettings' => [
          'sliderConfig' => ['config' => $slide_config],
          'sliderDetails' => $js_config,
        ],
      ],
    ];
  }

}
